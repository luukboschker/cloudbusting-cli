# Cloudbusting CLI #

The CloudBusting CLI is used to create and publish blueprint indexes for https://app.cloudbusting.it.

## Usage



## Development guidelines

After cloning the repository, you can run and debug the CLI in Visual Studio Code.

Or, make the executable link to your local repository by executing `npm link`.

After that, run `npm run watch` to start typescript compilation.

You can now the CLI using the `clb` command.

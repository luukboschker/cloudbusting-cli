# yaml-language-server: $schema=https://app.cloudbusting.it/assets/schemas/blueprint-v2.0.schema.json
---
name: "<%= name %>"
description: An example blueprint
version: "1.0.0"
keywords:
  - cloudbusting-blueprint  # Used by validations/extensions to determine this is a blueprint. Leave it.
  - aws
  - example
resourceTypes:
  # The following items show what is possible. Adapt and expand as needed.
  # Use an editor with a proper JSON Schema validation pkugin (like VSCode with the YAML plugin) for hints, validation and code-completion.
  alb:
    name: Application load balancer
    description: An application load balancer is the entrypoint to your infrastructure. It sends traffic to connected resources.
    rules:
      allowedConnections:
        ecs-service:
          description: Redirects traffic to Docker containers running as an ECS service. The connection name is used as the path prefix.
          named: true
        lambda:
          description: Redirects traffic to these Lambdas, via an API gateway proxy. The connection name is used as the path prefix.
          named: true

  fargate-cluster:
    name: Fargate cluster
    rules:
      allowedChildren:
        - ecs-service

  ecs-service:
    name: ECS service
    description: An ECS service that runs a Docker container.
    rules:
      childOnly: true
      allowedConnections:
        dynamodb-table:
          description: Allows the service to perform read/write operations on these tables.
        sns-topic:
          description: Allows the service to publish to these topics.
        sqs-queue:
          description: Allows the service to directly write to these queues.
        rds:
          description: Allows the service to access these databases.

  lambda:
    name: Lambda function
    rules:
      settings:
        runtime:
          description: The runtime used by this Lambda.
          select:
            required: true
            options:
              - dotnet3.1
              - node
      allowedConnections:
        dynamodb-table:
          description: Allows the service to perform read/write operations on these tables.
        sns-topic:
          description: Allows the service to publish to this topic.
        sqs-queue:
          description: Allows the service to directly write to this queue.
        rds:
          description: Allows the service to access these databases.

  dynamodb-table:
    name: DynamoDB table
    rules:
      settings:
        partitionkey:
          scalar:
            type: string
            required: true
        partitionkeyType:
          select:
            required: true
            options:
              - S
              - N
              - B
        sortkey:
          scalar:
            type: string
        sortkeyType:
          select:
            options:
              - S
              - N
              - B
        ttl:
          scalar:
            type: string
        globalsecondaryindexes:
          list:
            name:
              scalar:
                type: string
                required: true
            partitionkey:
              scalar:
                type: string
                required: true
            partitionkeyType:
              select:
                required: true
                options:
                  - S
                  - N
                  - B
            sortkey:
              scalar:
                  type: string
            sortkeyType:
              select:
                options:
                  - S
                  - N
                  - B

  rds:
    name: Postgresql database

  sns-topic:
    name: SNS topic
    rules:
      allowedConnections:
        sqs-queue:
          description: SQS queues to subscribe to this topic.
          settings:
            filter:
              description: Only messages that match the filter will be forwarded.
              scalar:
                type: string

  sqs-queue:
    name: SQS queue
    rules:
      settings:
        existing:
          description: Indicates that this queue already exists in another infraplan. It will not be created.
          scalar:
            type: boolean
      allowedConnections:
        lambda:
          description: Lambdas that will trigger on messages published on this queue.
          settings:
            batchSize:
              description: The maximum number of messages that the lambda will pick up for each run.
              scalar:
                required: true
                type: number

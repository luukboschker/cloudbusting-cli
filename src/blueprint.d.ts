/* eslint-disable no-use-before-define */
export interface Blueprint {
  $schema: string;
  name: string;
  version: string;
  description?: string;
  keywords?: string[];
  resourceTypes: ResourceTypes
}
/**
 * The object key is the resource ID, it's value the resource definition.
 */
type ResourceTypes = { [key: string]: ResourceType; }
export interface ResourceType {
  name: string;
  description?: string;
  rules?: Rules;
}

export interface Rules {
  allowedConnections?: AllowedConnections;
  allowedChildren?: string[];
  childOnly?: boolean;
  settings?: Settings;
}

/**
 * The object key references a connected resource ID. It's value is the connection  definition.
 */
type AllowedConnections = { [key: string]: AllowedConnection; }
export interface AllowedConnection {
  named?: boolean;
  description?: string;
  settings?: Settings;
}

/**
 * The object key references a setting name. It's value is the setting definition.
 */
type Settings = { [key: string]: Setting; }
export interface Setting {
  description?: string;
  scalar?: Scalar;
  select?: Select;
  list?: List;
}

/* eslint-disable no-use-before-define */
export interface Scalar {
  type: 'string' | 'number' | 'boolean';
  required?: boolean;
  pattern?: string;
}

export interface Select {
  required?: boolean;
  options: string[]
}

export interface List extends Settings {
}

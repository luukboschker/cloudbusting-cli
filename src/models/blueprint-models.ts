export interface BlueprintDefinition {
  name: string;
  version: string;
  description?: string;
  keywords?: string[];
  path: string;
}

export interface BlueprintIndex {
  name: string;
  description: string;
  homepage: string;
  blueprints: BlueprintDefinition[];
}

export interface BlueprintIndexPackage {
  name: string;
  description: string;
  homepage: string;
  keywords: string[];
}

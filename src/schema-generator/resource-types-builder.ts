import { JSONSchema7Definition } from 'json-schema';
import { ResourceTypes } from '../blueprint';
import { ResourceTypeBuilder } from './resource-type-builder';

export class ResourceTypesBuilder {
  private resourceTypes: ResourceTypes;

  constructor(resourceTypes: ResourceTypes) {
    if (!resourceTypes) throw new Error('No resourceType specified');
    this.resourceTypes = resourceTypes;
  }

  build(): JSONSchema7Definition[] {
    return Object.keys(this.resourceTypes)
      .map((id) => ({ id, resourceType: this.resourceTypes[id] }))
      .map(({ id, resourceType }) => new ResourceTypeBuilder(id, resourceType).build());
  }
}

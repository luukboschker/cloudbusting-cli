import { JSONSchema7Definition } from 'json-schema';
import { Select } from '../blueprint';
import { SettingBuilder } from './setting-builder';

export class SelectSettingBuilder implements SettingBuilder {
  private select: Select;

  constructor(select: Select) {
    if (!select) throw new Error('No select specified');
    this.select = select;
  }

  build(): JSONSchema7Definition {
    return {
      type: 'string',
      enum: [...this.select.options]
    };
  }

  isRequired(): boolean {
    return this.select.required ?? false;
  }
}

import { JSONSchema7Definition } from 'json-schema';
import { ResourceType } from '../blueprint';
import { AllowedConnectionsBuilder } from './allowed-connections-builder';
import { ChildrenBuilder } from './children-builder';
import { SettingsBuilder } from './settings-builder';

export class ResourceTypeBuilder {
  private id: string;

  private resourceType: ResourceType;

  constructor(id: string, resourceType: ResourceType) {
    this.id = id;

    if (!resourceType) throw new Error('No resourceType defined.');
    this.resourceType = resourceType;
  }

  build(): JSONSchema7Definition {
    const schema: JSONSchema7Definition = {};

    schema.properties = {};
    schema.properties[this.id] = this.buildResourceProperties();
    schema.additionalProperties = false;

    return schema;
  }

  private buildResourceProperties(): JSONSchema7Definition {
    const schema: JSONSchema7Definition = {
      type: 'object',
      description: this.resourceType.description || this.resourceType.name,
      additionalProperties: false,
    };

    schema.properties = {
      name: {
        type: 'string'
      },
    };
    schema.required = ['name'];

    const allowedConnections = this.resourceType.rules?.allowedConnections;
    if (allowedConnections) {
      schema.properties.connections = new AllowedConnectionsBuilder(allowedConnections).build();
      schema.required = [...schema.required, 'connections'];
    }

    const allowedChildren = this.resourceType.rules?.allowedChildren;
    if (allowedChildren) {
      schema.properties.children = new ChildrenBuilder(this.resourceType.name).build();
      schema.required = [...schema.required, 'children'];
    }

    const settings = this.resourceType.rules?.settings;
    if (settings) {
      schema.properties.settings = new SettingsBuilder(settings).build();
      schema.required = [...schema.required, 'settings'];
    }

    return schema;
  }
}

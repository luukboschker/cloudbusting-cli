import { JSONSchema7Definition } from 'json-schema';
import { List } from '../blueprint';
import { SettingBuilder } from './setting-builder';
import { SettingsBuilder } from './settings-builder';

export class ListSettingBuilder implements SettingBuilder {
  private list: List;

  constructor(list: List) {
    if (!list) throw new Error('No list specified');
    this.list = list;
  }

  build(): JSONSchema7Definition {
    return {
      type: 'array',
      items: new SettingsBuilder(this.list).build()
    };
  }

  // eslint-disable-next-line class-methods-use-this
  isRequired() {
    return false;
  }
}

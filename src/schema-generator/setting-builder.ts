import { JSONSchema7Definition } from 'json-schema';

export interface SettingBuilder {
  build(): JSONSchema7Definition;
  isRequired(): boolean;
}

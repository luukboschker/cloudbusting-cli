import { JSONSchema7Definition } from 'json-schema';
import { Scalar } from '../blueprint';
import { SettingBuilder } from './setting-builder';

export class ScalarSettingBuilder implements SettingBuilder {
  private scalar: Scalar;

  constructor(scalar: Scalar) {
    if (!scalar) throw new Error('No scalar specified');
    this.scalar = scalar;
  }

  build(): JSONSchema7Definition {
    return {
      type: this.scalar.type,
      pattern: this.scalar.pattern
    };
  }

  isRequired() {
    return this.scalar.required ?? false;
  }
}

import { JSONSchema7Definition } from 'json-schema';
import { Settings } from '../blueprint';
import { ListSettingBuilder } from './list-setting-builder';
import { ScalarSettingBuilder } from './scalar-settings-builder';
import { SelectSettingBuilder } from './select-setting-builder';
import { SettingBuilder } from './setting-builder';

export class SettingsBuilder {
  private settings: Settings;

  constructor(settings: Settings) {
    if (!settings) throw new Error('No settings specified');
    this.settings = settings;
  }

  build(): JSONSchema7Definition {
    const schema: JSONSchema7Definition = {};
    schema.type = 'object';
    schema.properties = {};
    schema.additionalProperties = false;
    schema.required = [];

    // eslint-disable-next-line no-restricted-syntax
    for (const id of Object.keys(this.settings)) {
      const settingBuilder = this.determineBuilder(id);

      schema.properties[id] = settingBuilder.build();

      if (this.settings[id].description) {
        schema.description = this.settings[id].description;
      }

      if (settingBuilder.isRequired()) {
        schema.required = [...schema.required, id];
      }
    }

    return schema;
  }

  private determineBuilder(id: string): SettingBuilder {
    const setting = this.settings[id];

    if (setting.select) {
      return new SelectSettingBuilder(setting.select);
    }
    if (setting.scalar) {
      return new ScalarSettingBuilder(setting.scalar);
    }
    if (setting.list) {
      return new ListSettingBuilder(setting.list);
    }

    throw new Error(`${id} has an nsupported setting type.`);
  }
}

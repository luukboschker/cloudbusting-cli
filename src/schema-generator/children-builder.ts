import { JSONSchema7Definition } from 'json-schema';

export class ChildrenBuilder {
  private resourceTypeName: string;

  constructor(resourceTypeName: string) {
    this.resourceTypeName = resourceTypeName;
  }

  // eslint-disable-next-line class-methods-use-this
  build(): JSONSchema7Definition {
    return {
      type: 'array',
      uniqueItems: true,
      description: `The resources that can be placed in this ${this.resourceTypeName}.`
    };
  }
}

import { JSONSchema7Definition } from 'json-schema';
import { Blueprint } from '../blueprint';
import logger from '../tools/logger';
import { ResourceTypesBuilder } from './resource-types-builder';

export class JsonSchemaBuilder {
  private schemaName: string;

  private blueprint: Blueprint;

  constructor(schemaName: string, blueprint: Blueprint) {
    this.schemaName = schemaName;
    this.blueprint = blueprint;
  }

  build(): JSONSchema7Definition {
    logger.info(`Generating JSON schema ${this.schemaName}`);

    const schema: JSONSchema7Definition = {
      $schema: 'http://json-schema.org/draft-07/schema',
      $id: `https://app.cloudbusting.it/assets/schemas/${this.schemaName}`,
      properties: {
        $schema: {
          type: 'string'
        },
        name: {
          type: 'string'
        },
        description: {
          type: 'string'
        },
        resources: {
          type: 'object',
          patternProperties: {
            '^[a-zA-Z0-9_-]*$': {
              type: 'object',
              description: 'ID of the resource',
              oneOf: new ResourceTypesBuilder(this.blueprint.resourceTypes).build()
            }
          }
        }
      },
      required: ['name', 'resources'],
      additionalProperties: false
    };

    return schema;
  }
}

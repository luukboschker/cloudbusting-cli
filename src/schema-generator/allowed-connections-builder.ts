import { JSONSchema7Definition } from 'json-schema';
import { AllowedConnections } from '../blueprint';
import { SettingsBuilder } from './settings-builder';

export class AllowedConnectionsBuilder {
  private allowedConnections: AllowedConnections;

  constructor(allowedConnections: AllowedConnections) {
    if (!allowedConnections) throw new Error('No allowedConnections specified');
    this.allowedConnections = allowedConnections;
  }

  build(): JSONSchema7Definition {
    const schema: JSONSchema7Definition = {};
    schema.type = 'object';
    schema.properties = {};
    schema.additionalProperties = false;

    // eslint-disable-next-line no-restricted-syntax
    for (const id of Object.keys(this.allowedConnections)) {
      schema.properties[id] = this.buildConnection(id);
    }

    return schema;
  }

  // eslint-disable-next-line class-methods-use-this
  private buildConnection(id: string): JSONSchema7Definition {
    const allowedConnection = this.allowedConnections[id];

    const schema: JSONSchema7Definition = {};
    schema.type = 'array';
    schema.items = {};
    schema.items.properties = {};
    schema.items.properties.target = { type: 'string' };
    schema.items.additionalProperties = false;
    schema.items.required = ['target'];

    if (allowedConnection.description) {
      schema.description = allowedConnection.description;
    }

    if (allowedConnection.named === true) {
      schema.items.properties.name = {
        type: 'string'
      };
      schema.items.required = [...schema.items.required, 'name'];
    }

    if (allowedConnection.settings) {
      const builder = new SettingsBuilder(allowedConnection.settings);
      schema.items.properties.settings = builder.build();
      schema.items.required = [...schema.items.required, 'settings'];
    }

    return schema;
  }
}

import { Argv } from 'yargs';

exports.command = 'new <command>';
exports.description = 'Create new blueprint artifacts';
exports.builder = (yargs: Argv) => yargs.commandDir('new_commands');

exports.handler = () => { };

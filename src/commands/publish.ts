import path from 'path';
import fs from 'fs-extra';
import { Argv } from 'yargs';
import logger from '../tools/logger';
import { isBlueprintIndex, readPackageDefinition } from '../tools/index-directory';
import { createBlueprintDirectory, isBlueprintDirectory } from '../tools/blueprint-directory';
import { BlueprintIndex } from '../models/blueprint-models';

exports.command = 'publish';
exports.description = 'Create a public directory for this index that can be hosted on a webserver.';
exports.builder = (yargs: Argv) => yargs
  .option('output', {
    alias: 'o',
    description: 'The name of the directory to publish in',
    default: './public'
  });

exports.handler = (args: any) => {
  const packageDefinition = readPackageDefinition('.');
  if (!packageDefinition || !isBlueprintIndex(packageDefinition)) {
    logger.error('Blueprint not published.');
    return;
  }

  const targetPath = path.resolve(args.output);
  const to = path.relative('.', targetPath);

  logger.info(`Publishing blueprint index ${packageDefinition.name} to ${to}`);

  fs.ensureDirSync(targetPath);
  fs.emptyDirSync(targetPath);

  const blueprintDirs = fs
    .readdirSync('.', { withFileTypes: true })
    .filter((entry) => entry.isDirectory() && isBlueprintDirectory(entry.name));

  const blueprints = blueprintDirs
    .map((blueprint) => createBlueprintDirectory(blueprint, targetPath));

  logger.info(`Create blueprint index ${packageDefinition.name}`);

  const blueprint: BlueprintIndex = {
    name: packageDefinition.name,
    description: packageDefinition.description,
    homepage: packageDefinition.homepage,
    blueprints
  };

  fs.writeJsonSync(`${targetPath}/index.json`, blueprint, { spaces: 2 });
};

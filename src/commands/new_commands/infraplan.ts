import path from 'path';
import { Argv } from 'yargs';
import logger from '../../tools/logger';
import { prepareDirectory } from '../../tools/prepare-directory';
import createDirectoryFromTemplate from '../../tools/template-directory';

exports.command = 'infraplan [name]';
exports.description = 'Create a new blueprint';
exports.builder = (argv: Argv) => argv
  .demandOption('name', 'Please specify the name of the new blueprint')
  .option('blueprint', {
    alias: 'b',
    description: 'The URL to the blueprint to use',
    demandOption: true
  });

exports.handler = (args: any) => {
  const { name, blueprint } = args;

  const targetPath = path.resolve(name);

  if (!prepareDirectory(targetPath)) {
    logger.error('Infraplan not created.');
    return;
  }

  logger.info(`Create new infraplan ${name} based on ${blueprint}`);

  const templatePath = path.resolve(path.join(__dirname, '../../../templates/infraplan'));
  createDirectoryFromTemplate({
    targetPath,
    templatePath,
    templateVariables: {
      name,
      blueprint
    }
  });
};

import * as path from 'path';
import { Argv } from 'yargs';
import createDirectoryFromTemplate from '../../tools/template-directory';
import logger from '../../tools/logger';
import { prepareDirectory } from '../../tools/prepare-directory';

exports.command = 'index';
exports.description = 'Create a new blueprint index repository';
exports.builder = (argv: Argv) => argv
  .option('output', {
    alias: 'o',
    description: 'Directory to create the index in. Uses the current directory if not specified.',
    default: '.'
  });

exports.handler = (args: any) => {
  const targetPath = path.resolve(args.output);
  const indexName = path.basename(targetPath);

  logger.info(`Create new blueprint index ${indexName}`);

  if (!prepareDirectory(targetPath)) {
    logger.error('Blueprint index not created.');
    return;
  }

  const templatePath = path.resolve(path.join(__dirname, '../../../templates/index'));
  createDirectoryFromTemplate({
    targetPath,
    templatePath,
    templateVariables: {
      indexName
    }
  });
};

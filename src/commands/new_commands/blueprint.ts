import path from 'path';
import { Argv } from 'yargs';
import createDirectoryFromTemplate from '../../tools/template-directory';
import { isBlueprintIndexDirectory } from '../../tools/index-directory';
import logger from '../../tools/logger';
import { prepareDirectory } from '../../tools/prepare-directory';

exports.command = 'blueprint [name]';
exports.description = 'Create a new blueprint';
exports.builder = (argv: Argv) => argv
  .demandOption('name', 'Please specify the name of the new blueprint');

exports.handler = (args: any) => {
  if (!isBlueprintIndexDirectory('.')) {
    logger.error('Blueprint not created.');
    return;
  }

  const { name } = args;
  const targetPath = path.resolve(name);

  if (!prepareDirectory(targetPath)) {
    logger.error('Blueprint index not created.');
    return;
  }

  logger.info(`Create new blueprint ${name}`);

  const templatePath = path.resolve(path.join(__dirname, '../../../templates/blueprint'));
  createDirectoryFromTemplate({
    targetPath,
    templatePath,
    templateVariables: {
      name
    }
  });
};

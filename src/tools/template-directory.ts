import * as fs from 'fs-extra';
import * as path from 'path';
import * as ejs from 'ejs';
import logger from './logger';

interface Arguments {
  targetPath: string;
  templatePath: string;
  templateVariables?: any
  skipFiles?: string[];
}

export default function createDirectoryFromTemplate(args: Arguments) {
  if (!path.isAbsolute(args.targetPath)) throw new Error('targetPath must be absolute');
  if (!path.isAbsolute(args.templatePath)) throw new Error('templatePath must be absolute');

  const skipFiles = args.skipFiles || [];
  const templateVariables = args.templateVariables || [];

  const templates = fs.readdirSync(args.templatePath);
  templates.forEach((file) => {
    const template = path.join(args.templatePath, file);

    if (skipFiles.includes(file)) return;

    const stat = fs.statSync(template);
    if (stat.isFile()) {
      // read file content and transform it using template engine
      let contents = fs.readFileSync(template, 'utf8');
      contents = ejs.render(contents, templateVariables);

      const writePath = path.join(args.targetPath, file);
      const to = path.relative(process.cwd(), writePath);
      logger.debug(`Creating file ${to}`);

      fs.writeFileSync(writePath, contents, 'utf8');
    } else if (stat.isDirectory()) {
      // Recursively process this directory
      fs.mkdirSync(path.join(args.targetPath, file));
      createDirectoryFromTemplate({
        targetPath: path.join(args.targetPath, file),
        templatePath: path.join(args.templatePath, file),
        skipFiles,
        templateVariables
      });
    }
  });
}

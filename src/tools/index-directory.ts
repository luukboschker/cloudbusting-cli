import fs from 'fs';
import { BlueprintIndexPackage } from '../models/blueprint-models';
import logger from './logger';

export function readPackageDefinition(dir: string): BlueprintIndexPackage | undefined {
  const packageFile = `${dir}/package.json`;
  if (!fs.existsSync(packageFile)) {
    logger.error('No package.json in current directory. Is this a blueprint index?');
    return undefined;
  }

  return JSON.parse(fs.readFileSync(packageFile, { encoding: 'utf-8' }));
}

export function isBlueprintIndex(packageDefinition: BlueprintIndexPackage): boolean {
  if (!packageDefinition.keywords || !packageDefinition.keywords.includes('blueprintindex')) {
    logger.error('package definition does not contain the \'blueprintindex\' keyword. Is this a blueprint index?');
    return false;
  }

  return true;
}

export function isBlueprintIndexDirectory(dir: string): boolean {
  const packageDefinition = readPackageDefinition(dir);
  if (!packageDefinition) return false;

  return isBlueprintIndex(packageDefinition);
}

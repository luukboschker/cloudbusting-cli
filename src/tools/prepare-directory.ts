import * as fs from 'fs-extra';
import * as path from 'path';
import logger from './logger';

export function prepareDirectory(targetPath: string): boolean {
  if (!path.isAbsolute(targetPath)) throw new Error('targetPath must be absolute');

  const to = path.relative(process.cwd(), targetPath) || '.';

  logger.debug(`Preparing output directory ${to}`);

  if (fs.pathExistsSync(targetPath) && fs.readdirSync(targetPath).length > 0) {
    logger.error(`Output directory ${to} is not empty`);
    return false;
  }

  fs.mkdirSync(targetPath, { recursive: true });

  return true;
}

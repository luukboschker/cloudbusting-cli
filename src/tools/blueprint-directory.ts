import fs from 'fs-extra';
import path from 'path';
import * as yaml from 'js-yaml';
import { BlueprintDefinition } from '../models/blueprint-models';
import logger from './logger';
import { JsonSchemaBuilder } from '../schema-generator/json-schema-builder';
import { Blueprint } from '../blueprint';

export function isBlueprintDirectory(dir: string): boolean {
  const blueprintFile = `${dir}/blueprint.yaml`;
  if (!fs.existsSync(blueprintFile)) {
    logger.debug(`${blueprintFile} not present in directory.`);
    return false;
  }

  return true;
}

export function createBlueprintDirectory(dir: fs.Dirent, targetPath: string): BlueprintDefinition {
  const blueprintFile = `${dir.name}/blueprint.yaml`;
  if (!fs.existsSync(blueprintFile)) {
    throw new Error(`${blueprintFile} is not a blueprint directory; skipping it.`);
  }

  const blueprint = yaml.load(fs.readFileSync(blueprintFile, 'utf-8')) as Blueprint;

  logger.info(`Publishing ${blueprint.name} in ${dir.name}`);
  fs.copySync(dir.name, path.join(targetPath, dir.name));

  const schemaName = `${blueprint.name}.schema.json`;
  const schemaBuilder = new JsonSchemaBuilder(schemaName, blueprint);
  const schema = schemaBuilder.build();
  fs.writeJSONSync(path.join(targetPath, dir.name, schemaName), schema, { spaces: 2 });

  return {
    name: blueprint.name,
    version: blueprint.version,
    description: blueprint.description,
    keywords: blueprint.keywords,
    path: dir.name,
  };
}

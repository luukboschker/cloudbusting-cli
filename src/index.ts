import * as yargs from 'yargs';
import logger from './tools/logger';

// eslint-disable-next-line no-unused-vars
const { argv } = yargs
  .commandDir('commands')
  .option('verbose', {
    description: 'Show debug information',
    alias: 'v',
    type: 'boolean',
    coerce: () => { logger.level = 'debug'; }
  })
  .demandCommand()
  .recommendCommands()
  .help();
